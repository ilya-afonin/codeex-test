import Vue from 'vue'
import App from './App.vue'
import {
  MdButton,
  MdTable,
  MdContent,
  MdApp,
  MdCard,
  MdRipple,
  MdIcon,
  MdDialog,
  MdField,
  MdAutocomplete,
  MdMenu,
  MdList,
  MdProgress,
  MdHighlightText
} from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.use(MdButton)
Vue.use(MdTable)
Vue.use(MdContent)
Vue.use(MdApp)
Vue.use(MdCard)
Vue.use(MdRipple)
Vue.use(MdIcon)
Vue.use(MdDialog)
Vue.use(MdField)
Vue.use(MdAutocomplete)
Vue.use(MdMenu)
Vue.use(MdList)
Vue.use(MdProgress)
Vue.use(MdHighlightText)

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
