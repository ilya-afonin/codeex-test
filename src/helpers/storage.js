const storage = {
  get (key, defaultValue) {
    return localStorage.getItem(`codeex_test_${key}`) ? JSON.parse(localStorage.getItem(`codeex_test_${key}`)) : defaultValue
  },
  set (key, value) {
    return new Promise(() => {
      localStorage.setItem(`codeex_test_${key}`, JSON.stringify(value))
    })
      .then(() => {
        success()
      })
      .catch(() => {
        error()
      })
  }
}

function success () {
  alert('Запись сохранена успешно')
}

function error () {
  alert('Не удалось сохранить запись')
}

export default storage
